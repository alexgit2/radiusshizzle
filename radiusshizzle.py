# Andrew Mulheirn - Feb 2020
# This script prints to screen the commands to create a lot of RADIUS clients in Microsoft NPS.
# This configuration must be manually pasted into a command prompt on the NPS servers.
#
# After that, it connects to each Juniper switch and applies the appropriate configuration
# with the right shared key, source IP address etc.

import csv
import getpass
from jnpr.junos import Device
from jnpr.junos.utils.config import Config

deviceUsername = input("Enter your username: ")
devicePassword = getpass.getpass(prompt="Enter your password: ")

with open('devices.csv') as csvfile:
    devices = list(csv.reader(csvfile))

print("RADIUS details - note that special characters (e.g. &, £, $) must be "
      "escaped for Microsoft's benefit using a backtick (`) character\n\n")

secret = input("Enter the RADIUS shared secret: ")
radiusServer1 = input("Enter the first RADIUS server IP address: ")
radiusServer2 = input("Enter the second RADIUS server IP address: ")

# strip any escape characters out of the secret so we don't apply then to Junos
junossecret = secret.replace('`','')

print('======= NPS configuration ======')
for device in devices:
    # NOTE - the Powershell command below somehow breaks the shared secret.
    # Even though you put it in right and when you do a Get-NpsRadiusCient it
    # shows up ok, it will not accept the RADIUS client until you go into the
    # GUI and type the shared secret in again!
    # print("Set-NpsRadiusClient -name {} -Address {} -SharedSecret {}".format(device[0], device[1], secret))
    # Instead, use the following netsh command
    print('netsh nps add client name=NET-{} address={} state=Enable sharedsecret={}'.format(device[0], device[1], secret))

print("================================\n")
print("Now please paste the above commands "
      "into a command prompt in your NPS servers\n")
print('===== END NPS configuration ====\n\n')





print("======= Junos Config =====\n")

for device in devices:
    with Device(device[1], user=deviceUsername, password=devicePassword, port=22) as dev:
        print("Connected to {}...".format(device[0]))
        # Create a configuration template in which to insert the variables
        configTemplate = 'set system radius-server {} port 1812\n' \
                        'set system radius-server {} secret {}\n' \
                        'set system radius-server {} source-address {}\n' \
                        'set system radius-server {} retry 3\n' \
                        'set system radius-server {} port 1812\n' \
                        'set system radius-server {} secret {}\n' \
                        'set system radius-server {} source-address {}\n' \
                        'set system radius-server {} retry 3\n'\
                        'set system login user SU class super-user\n'\
                        'set system authentication-order radius\n'\
                        'set system authentication-order password\n'\
                        'set system radius-options password-protocol mschap-v2'

        # Insert the variables into the template and store in configSnippet
        configSnippet = configTemplate.format(radiusServer1, radiusServer1, junossecret, radiusServer1, device[1], radiusServer1, radiusServer2, radiusServer2, junossecret, radiusServer2, device[1], radiusServer2)

        # Connect to the device and load the config
        cu = Config(dev)
        cu.load(configSnippet, format="set", merge=True)

        # Print to screen if desired
        #print(configSnippet.format(radiusServer1, radiusServer1, junossecret, radiusServer1, device[1], radiusServer1, radiusServer2, radiusServer2, junossecret, radiusServer2, device[1], radiusServer2))

        # Perform a show | compare to check you didn't blat anything
        cu.pdiff()

        # Ask user if they want to commit or not
        goAhead = input("Do you want to commit (y/n): ")
        if goAhead == 'y':
            cu.commit()
        else:
            dev.close()
